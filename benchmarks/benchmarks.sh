#!/bin/bash

# Imitator binary
imitator="${HOME}/Work/code/imitator/imitator/bin/imitator"

# maude binary
maude="maude.linux64 -no-banner -batch"

# pta2maude parser
BASE_DIR="$(dirname "$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)")"
parser="${BASE_DIR}/pta2maude/app.py"

# folder containing the folder
model_folder="models"

# modes
modes=("collapsing" "no-collapsing")


# run imitator
function run_imitator {
  local model=$1
  local property=$2
  local timeout_value=$3
  timeout $timeout_value $imitator "${model}" "${property}" -output-prefix "${property}"
}

# run parser pta2maude
function parser_files {
  local model=$1
  local mode=$2
  echo "parsing" $name "with mode" "$mode" "..."
  python3 $parser --input "${model_folder}/${model}.imi" --output "${model_folder}/${model}.${mode}.maude" "--${mode}"
}

# run Maude
function run_maude {
  local filename=$1
  local timeout_value=$2
  timeout "$timeout_value" ${maude} "${filename}"
}

# create a folder
function create_folder {
  local path=$1

  if [ ! -d "$path" ]; then
    mkdir -p "$path";
  fi
}

function run_benchmark {
  local model=$1
  local timeout=$2

  # create folder to save the output of the model
  output_folder="logs/${model}"
  create_folder "${output_folder}"
  cp "${model_folder}/pta-base.maude" "${model_folder}/meta-pta.maude" "${output_folder}"

  # 1) generate the full model with parser
  for mode in ${modes[@]}; do
    parser_files $model "$mode"
  done

  # 2) get the locations of the full model
  initial_loc=".*"
  locations=$(cat ${model_folder}/${model}.imi.full | grep "loc ${initial_loc}\s*:" | cut -d: -f1 | awk '{ print $NF }')

  # imitator files
  prop_file="${model}-EFwitness.imiprop"
  model_path="${model_folder}/${model}.imi.full"

  for l in $locations; do
    echo "reaching location:" $l

    # 3) run imitator
    new_prop_path="${output_folder}/${prop_file}.${l}"
    if [[ ! -f "${new_prop_path}.res" ]]; then
      sed "s/<replace>/${l}/" "${model_folder}/${prop_file}" > "${new_prop_path}"
      echo "Running ${model} with ${new_prop_path}"
      run_imitator "${model_path}" "${new_prop_path}" "${timeout}"
    fi

    # 4) run maude
    for mode in ${modes[@]}; do

      # maude files
      maude_file="${model}.${mode}.maude"
      echo $maude_file
      new_file="${output_folder}/${maude_file}.${l}"

      if [[ ! -f "${new_file}" ]]; then
        sed "s/<replace>/${l}/" "${model_folder}/${maude_file}" > "${new_file}"
        echo "Running Maude with ${new_file}"
        run_maude "${new_file}" "${timeout}" > "${new_file}.res"
      fi
    done

  done

  # 5) generate file with locations where maude finishes
  location_file="${output_folder}/locations.txt"

  files=$(cd "${output_folder}" && grep -Rli "rewrite" ".")
  success_locations=$(echo "$files"| tr " " "\n"| cut -d. -f5 | sort -u)
  echo "$success_locations" > "${location_file}"
  sed -zi 's/\n$//g' ${location_file}
  sed -zi 's/^\n//g' ${location_file}
}

# Print a help message.
function usage() {
  echo "Usage: $0 -m MODEL -t TIMEOUT" 1>&2
}

# Exit with error.
exit_abnormal() {
  usage
  exit 1
}

# -----------------------------------------------------------------------------
# MAIN
# -----------------------------------------------------------------------------

TIMEOUT=""
MODEL=""

while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -m|--model) MODEL="$2"; shift ;;
    -t|--timeout) TIMEOUT="$2"; shift ;;
    *) exit_abnormal ;;
  esac
  shift
done

if [ "$MODEL" = "" ] || [ "$TIMEOUT" = "" ]; then
  exit_abnormal
fi

# run benchmark
run_benchmark "${MODEL}" "${TIMEOUT}"