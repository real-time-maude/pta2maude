# pta2maude

We give a rewriting logic semantics for parametric timed automata (PTAs) and
show that symbolic reachability analysis using
[Maude-with-SMT](https://maude-se.github.io/) is sound and complete for the PTA
reachability problem. We refine standard Maude-with-SMT reachability so that
the analysis terminates when the symbolic state space of the PTA is finite.
Besides reachability, the rewrite theory can be also used for parameter
synthesis. This repository includes benchmarks comparing our methods and
[Imitator](https://imitator.lipn.univ-paris13.fr/).

Details about the translation can be found in [this paper](./paper.pdf).

## Getting started

The project was tested in [Maude 3.2](http://maude.cs.illinois.edu/) and [Maude
SE](https://maude-se.github.io/). A script written in [Python
3.10](https://www.python.org/) is used to parse Imitator input files into Maude
files.

### Maude files

The specification can be found in the folder `maude`. It includes the following files:

- _pta-base_: Sorts for defining locations, states and constrained terms.
- _meta-pta_: Reachability procedures with folding.
- _coffee.maude_: Example of the coffee machine using the command smt-search.
- _coffee-folding_: Example of the coffee machine using folding.
- _coffee-strategy_: Example using the strategy language to constraint the
  behavior of the PTA.
- _ex-fig3a_, _ex-fig3b_, _ex-fig3b-folding_: Examples in Figure 3 in the paper.

See the headers of each file for further information.

### Parser

The parser of Imitator models into Maude theories can be found in `pta2maude`.
See the file [README](./pta2maude/README.md) for further information about its
implementation in Python.

### Benchmarks

See the file [README](./benchmarks/README.md) for further information about
the benchmarks comparing the performance of Imitator and our analyses.
