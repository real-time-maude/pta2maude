import os

import pytest
from src.Parser import CollapsingParser, InterpreterParser, NoCollapsingParser


def remove_spaces(output_str):
    """Removes all the spaces in a string"""
    output = list(filter(None, [e.strip() for e in output_str.split("\n")]))
    return output


@pytest.mark.parametrize(
    "imitator_file,maude_file,version",
    [
        ("ex1x.imi", "ex1x.maude", "collapsing"),
        ("ex1xy.imi", "ex1xy.maude", "collapsing"),
        ("ex1x.imi", "ex1x-nc.maude", "no-collapsing"),
        ("ex1xy.imi", "ex1xy-nc.maude", "no-collapsing"),
        ("coffee.imi", "coffee-nc.maude", "no-collapsing"),
        ("coffee.imi", "coffee-intrpr.maude", "interpreter"),
        ("fischer.imi", "fischer-intrpr.maude", "interpreter"),
        ("train-intruder.imi", "train-intruder-intrpr.maude", "interpreter"),
    ],
)
def test_parser(examples_dir, imitator_file, maude_file, version):
    # select the correct parser
    if version == "collapsing":
        parser = CollapsingParser()
        sync_product = True
    elif version == "no-collapsing":
        parser = NoCollapsingParser()
        sync_product = True
    elif version == "interpreter":
        parser = InterpreterParser()
        sync_product = False
    else:
        raise Exception(f"the version '{version}' is not supported")

    # parse the imitator file
    input_file = os.path.join(examples_dir, imitator_file)
    output = parser.parse_file(input_file, sync_product)

    # read the file with the expected output
    expected_file = os.path.join(examples_dir, maude_file)
    expected = open(expected_file).read()

    assert remove_spaces(output) == remove_spaces(expected)
