import os
import re
import textwrap
from abc import ABC, abstractmethod

import antlr4
from src.dist.ImitatorLexer import ImitatorLexer
from src.dist.ImitatorParser import ImitatorParser
from src.Imitator import Automaton, Location, Model, Transition
from src.ImitatorVisitor import MyVisitor
from src.Maude import ImportType, Maude


class Parser(ABC):
    """
    An abstract class to represent the parser of Imitator

    Attributes
    ----------
    maude : Maude
        Object containing methods to generate Maude statements
    """

    def __init__(self) -> None:
        """Constructor of the class"""
        self.maude = Maude()

    @abstractmethod
    def to_maude(self, model: Model) -> str:
        """Parse Imitator model to Maude model"""
        pass

    def to_imitator(self, model: Model) -> str:
        """
        Parse an Imitator model with Imitator syntax

        Parameters
        ----------
        model : Model
            Imitatator model

        Returns
        -------
        str
        """
        imitator_str = "var"

        # parse clocks and parameters
        if len(model.clocks):
            clocks_str = ", ".join(model.clocks)
            imitator_str += "\n  {clocks} : clock;".format(clocks=clocks_str)
        if len(model.parameters):
            parameters_str = ", ".join(model.parameters)
            imitator_str += "\n  {parameters} : parameter;".format(
                parameters=parameters_str
            )

        # parse automata
        automata_str = ""
        for automaton in model.automata:
            name = automaton.name

            automaton_str = "\n\n"
            automaton_str += textwrap.dedent(
                f"""
            (************************************************************)
              automaton {name}
            (************************************************************)
            """
            ).strip()

            # parse actions
            if len(automaton.actions):
                synclabs_str = ", ".join(automaton.actions)
                automaton_str += "\nsynclabs: {synclabs};".format(synclabs=synclabs_str)

            # parse locations
            for loc in automaton.locations:
                accepting = "accepting " if loc.accepting else ""
                urgent = "urgent " if loc.urgent else ""

                automaton_str += (
                    f"\n\n{accepting}{urgent}loc {loc.name}: invariant {loc.invariant}"
                )

                # parse transitions
                for t in automaton.transitions_from(loc.name):
                    automaton_str += "\n  when {guard} ".format(guard=t.guard)

                    if t.sync is not None:
                        automaton_str += f"sync {t.sync} "

                    updates_str = ", ".join(t.update)
                    automaton_str += f"do {{{updates_str}}} goto {t.target};"

            automata_str += f"{automaton_str}\n\nend (* {name} *)"
        imitator_str += automata_str + "\n\n"

        initial_locations_str = "\n          ".join(
            [f"& loc[{a.name}] = {a.initial_location.name}" for a in model.automata]
        )

        initial_constraints_str = "\n          ".join(
            [f"& {c}" for c in model.initial_constraints]
        )

        imitator_str += textwrap.dedent(
            f"""
        (************************************************************)
        (* Initial state *)
        (************************************************************)

        init :=
          (*------------------------------------------------------------*)
          (* Initial location *)
          (*------------------------------------------------------------*)
          {initial_locations_str}

          (*------------------------------------------------------------*)
          (* Parameter constraints *)
          (*------------------------------------------------------------*)
          {initial_constraints_str}
        ;

        (************************************************************)
        (* The end *)
        (************************************************************)
        end
        """
        ).strip()

        return imitator_str

    def parse_file(self, filename: str, sync_product: bool) -> str:
        """
        Parse an Imitator file into a Maude model

        Parameters
        ----------
        filename : str
            Imitator file

        sync_product : bool
            Computes the synchronisation product of the automata

        Returns
        -------
        str
        """
        fs_in = antlr4.FileStream(filename, encoding="utf8")

        # lexer
        lexer = ImitatorLexer(fs_in)
        stream = antlr4.CommonTokenStream(lexer)

        # parser
        parser = ImitatorParser(stream)
        tree = parser.main()

        # evaluator
        visitor = MyVisitor()
        model = visitor.visit(tree)

        # compute synchronized product if there are more than 1 automaton
        if sync_product and len(model.automata) > 1:
            model_name = os.path.splitext(os.path.basename(filename))[0]
            model = model.get_synchronized_product(model_name.replace("-", "_"))
            with open(f"{filename}.full", "w") as f:
                f.write(self.to_imitator(model))

            assert len(model.automata) == 1, "Only 1 automaton is supported."

        # parse to Maude
        output = self.to_maude(model)

        return output


class NoCollapsingParser(Parser):
    """A class to represent the parser of Imitator to Maude using smt"""

    def __init__(self, var_type="Real") -> None:
        """
        Constructor of the class

        Parameters
        ----------
        var_type : str
            type of the variables
        """
        super().__init__()
        self.var_type = var_type
        self._time_var = "T"

    def _get_model_variables(self, model: Model) -> list[str]:
        """
        Get the variables of the model (clocks and parameters)

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
            list[str]
        """
        return model.clocks + model.parameters

    def _normalize_float(self, number: str) -> str:
        """Transforms float into rational"""
        new_number = number.replace(".", "")
        nb_decimals = len(new_number) - number.index(".")
        new_number += f"/1{'0'*nb_decimals}"

        return new_number.lstrip("0")

    def _normalize_numbers(self, constraint: str) -> str:
        """Add x/1 to the numbers in a constraint"""
        number = re.sub(r"((?<!\.|\/)\b[0-9]+\b(?!\.|\/))", r"\1/1", constraint)
        float = re.sub(
            r"(\d+\.\d+)", lambda g: self._normalize_float(g.group(1)), number
        )

        return float

    def _normalize_constraints(self, constraint: str) -> str:
        """
        Normalize the string represention of a constraint. For instance, adding
        spaces around binary operators.

        Parameters
        ----------
        constraint : str
            string representation of the constraint

        Returns
        -------
        str
        """
        # add space around binary operators
        c_with_spaces = re.sub(r"((?!\.|\/)\W+)", r" \1 ", constraint)

        # remove more than one space
        result = re.sub(" +", " ", c_with_spaces)

        # add x/1 to all the number
        fix_numbers = self._normalize_numbers(result)

        # replace binary & by and
        good_operators = (
            fix_numbers.replace(" | ", " or ")
            .replace(" & ", " and ")
            .replace(" = ", " === ")
        )

        return good_operators

    def _get_updated_variables(
        self, variables: list[str], updates: list[str]
    ) -> list[str]:
        """
        Return the list of variables with the updated value

        Parameters
        ----------
        variables : list[str]
            list of variables to be updated
        updates : list[str]
            list of updates

        Returns
        -------
        list[str]
        """
        new_vars = list(variables)
        for u in updates:
            matches = re.findall(r"(?P<var>\w+)\s*:=\s*(?P<val>\d+)", u)

            # applying updates
            for var, value in matches:
                new_vars[new_vars.index(var)] = value
        return new_vars

    def _get_preamble(
        self, automaton: Automaton, clocks: list[str], parameters: list[str]
    ) -> str:
        """
        Get the preamble of the Maude file

        Parameters
        ----------
        automaton : Automaton
            automaton of the model
        clocks : list[str]
            clocks of the model
        parameters : list[str]
            parameters of the model

        Returns
        -------
        str
        """
        # define sorts
        sorts = self.maude.sorts(["State", "Location"])

        # clock variables
        variables = [self.maude.variables(self._time_var, self.var_type)]
        if clocks:
            variables.append(self.maude.variables(clocks, self.var_type))
        if parameters:
            variables.append(self.maude.variables(parameters, self.var_type))

        variables_str = "\n            ".join(variables)

        # define location constructors
        locations = [loc.name for loc in automaton.locations]
        locations_str = self.maude.operators(locations, [], "Location", [])

        # source term
        nb_clocks = len(clocks)
        nb_parameters = len(parameters)
        term_domain = ["Location"] + [self.var_type] * (nb_clocks + nb_parameters)
        parameter_term = ";".join(["_"] * nb_parameters)
        left_side = ";".join(["_"] * (nb_clocks + 1))

        source_term = self.maude.operators(
            [f"<{left_side}> <{parameter_term}>"], term_domain, "State", []
        )
        target_term = self.maude.operators(
            [f"[{left_side}] <{parameter_term}>"], term_domain, "State", []
        )

        preamble = f"""
            {sorts}

            {variables_str}

            {locations_str}

            {source_term}
            {target_term}"""

        return preamble

    def _get_state(
        self,
        location: str,
        clocks: list[str],
        parameters: list[str],
        intermediate: bool,
    ) -> str:
        """ "
        Return the representation of an state in Maude

        Parameters
        ----------
        location : str
            identifier of an Imitator state
        clocks : list[str]
            list of clocks of the model
        parameters : list[str]
            list of parameters of the model
        intermediate : bool
            flag to indicate if it's an intermediate state

        Returns
        -------
        str
        """
        left_side_str = " ; ".join([location] + clocks)
        left_side_str = self._normalize_numbers(left_side_str)

        parameters_str = " ; ".join(parameters)
        parameters_str = f"< {parameters_str} >" if len(parameters) else "<>"

        left_side = f"< {left_side_str} >" if (intermediate) else f"[ {left_side_str} ]"

        state = f"{left_side} {parameters_str}"
        return state

    def _get_rule(
        self,
        from_s: Location,
        to_s: Location,
        guard: str,
        clocks: list[str],
        parameters: list[str],
        clock_updates: list[str],
    ) -> str:
        """
        Return a Maude rule for a transition

        Parameters
        ----------
        from_s : Location
            source location of the transition
        to_s : Location
            target location of the transition
        guard : str
            guard of the transition
        clocks: list[str]
            clocks of the model
        parameters: list[str]
            parameters of the model
        clock_updates : list[str]
            clocks updated in the transition

        Returns
        -------
        str
        """
        rl_name = f"{from_s.name}-{to_s.name}"

        source_conf = self._get_state(from_s.name, clocks, parameters, True)
        target_conf = self._get_state(to_s.name, clock_updates, parameters, False)

        # reset variables in invariant
        invariant = to_s.invariant if to_s.invariant != "True" else ""
        vars_in_condition = re.findall(r"(_*[A-Za-z][A-Za-z0-9_]*)", invariant)
        for var in vars_in_condition:
            if var in clocks:
                value = clock_updates[clocks.index(var)]
                if value.isnumeric():
                    invariant = re.sub(rf"\b{var}\b", f"{value}", invariant)

        # build constraint
        constraints = []
        if invariant != "":
            constraints.append(invariant)
        if guard is not None and guard != "True":
            constraints.append(guard)

        condition = " and ".join(constraints)
        condition = self._normalize_constraints(condition)

        condition = [f"({condition}) = true"] if condition != "" else []

        rule = self.maude.rule(rl_name, source_conf, target_conf, [], condition)
        return rule

    def _get_tick_rule(
        self, s: Location, clocks: list[str], parameters: list[str]
    ) -> str:
        """
        Return a Maude tick rule for a location

        Parameters
        ----------
        s : Location
            an model's location
        clocks : list[str]
            clocks of the model
        parameters : list[str]
            parameters of the model

        Returns
        -------
        str
        """
        state_name = s.name

        # build source configuration
        source_conf = self._get_state(state_name, clocks, parameters, False)

        # updated variables with clock + t
        new_clocks = [f"{clk} + {self._time_var}" for clk in clocks]

        # build target configuration
        target_conf = self._get_state(state_name, new_clocks, parameters, True)

        # add t to each variable in the invariant
        invariant = s.invariant if s.invariant != "True" else None
        if invariant is not None:
            for c in clocks:
                invariant = re.sub(rf"\b{c}\b", f"{c} + {self._time_var}", invariant)
            invariant = self._normalize_constraints(invariant)

        condition = " and ".join(filter(None, [f"{self._time_var} >= 0/1", invariant]))
        condition = f"({condition}) = true"

        rule = self.maude.rule(
            f"tick-{state_name}", source_conf, target_conf, ["nonexec"], [condition]
        )

        return rule

    def _get_search_cmd(
        self,
        initial_location: str,
        clocks: list[str],
        parameters: list[str],
        nb_traces: int = None,
        nb_steps: int = None,
    ) -> str:
        """
        Return the search command for reachability verification

        Parameters
        ----------
        initial_location: str
            Initial location of the model
        clocks : list[str]
            clocks of the model
        parameters : list[str]
            parameters of the model
        nb_traces : int
            number of traces to be found in the reachability analysis
        nb_steps : int
            number of steps of the reachability analysis

        Returns
        -------
        str
        """
        # initial state
        initial_state = self._get_state(initial_location, clocks, parameters, True)

        # final state
        fresh_clocks = [f"{c}':{self.var_type}" for c in clocks]
        final_state = self._get_state("<replace>", fresh_clocks, parameters, True)

        # initial conditions
        condition_1 = []
        if len(clocks) > 1:
            condition_1 = [f"{clocks[0]} === {c}" for c in clocks[1:]]

        condition_2 = [f"{v} >= 0/1" for v in clocks + parameters]
        condition = " and ".join(condition_1 + condition_2)
        condition = f" such that ( {condition} ) = true" if condition != "" else ""

        nb_traces_str = "" if (nb_traces is None) else f" [{nb_traces}]"
        nb_steps_str = "*" if (nb_steps is None) else f"{nb_steps}"

        cmd = f"smt-search{nb_traces_str} {initial_state} =>{nb_steps_str} {final_state}{condition} ."
        return cmd

    def to_maude(self, model: Model) -> str:
        """
        Parse an Imitator model into Maude

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        automaton = model.automata[0]
        clocks = model.clocks
        parameters = model.parameters

        initial_constraints = model.initial_constraints
        if len(initial_constraints):
            print("Please check the initial constraints in the search command:")
            print(f"    {initial_constraints}")

        rules = []
        for source in automaton.locations:
            for t in automaton.transitions_from(source.name):
                target = automaton.get_location(t.target)

                # update update variables
                updates = self._get_updated_variables(clocks, t.update)

                # add transition rule
                rules.append(
                    self._get_rule(source, target, t.guard, clocks, parameters, updates)
                )

            # add tick rule
            rules.append(self._get_tick_rule(source, clocks, parameters))
            rules.append("")

        rules = "\n            ".join(rules)

        load_smt = self.maude.loadFile("smt")
        import_real = self.maude.importModule("REAL", ImportType.PROTECTING)
        preamble = self._get_preamble(automaton, clocks, parameters)
        search_cmd = self._get_search_cmd(
            automaton.initial_location.name, clocks, parameters, 1
        )

        model_str = f"""\
        {load_smt}

        mod MODEL is
            {import_real}
            {preamble}

            {rules}
        endm

        {search_cmd}

        quit .

        eof
        """

        return textwrap.dedent(model_str).strip()


class CollapsingParser(NoCollapsingParser):
    """A class to represent the parser of Imitator to Maude that does not collapse states"""

    def __init__(self, var_type="Real") -> None:
        """
        Constructor of the class

        Parameters
        ----------
        var_type : str
            type of the variables
        """
        super().__init__(var_type)

    def _get_preamble(
        self, automaton: Automaton, clocks: list[str], parameters: list[str]
    ) -> str:
        """
        Get the preamble of the Maude file

        Parameters
        ----------
        automaton : Automaton
            automaton of the model
        clocks : list[str]
            clocks of the model
        parameters : list[str]
            parameters of the model

        Returns
        -------
        str
        """
        # clock variables
        variables = [self.maude.variables(self._time_var, self.var_type)]
        if clocks:
            variables.append(self.maude.variables(clocks, self.var_type))
        if parameters:
            variables.append(self.maude.variables(parameters, self.var_type))
        ""
        variables_str = "\n            ".join(variables)

        # define location constructors
        locations = [loc.name for loc in automaton.locations]
        locations_str = self.maude.operators(locations, [], "Location", [])

        # source term
        nb_clocks = len(clocks)
        nb_parameters = len(parameters)
        term_domain = ["Location"] + [self.var_type] * (nb_clocks + nb_parameters)
        parameter_term = ";".join(["_"] * nb_parameters)
        left_side = ";".join(["_"] * (nb_clocks + 1))

        source_term = self.maude.operators(
            [f"<{left_side}> <{parameter_term}>"], term_domain, "NState", []
        )
        target_term = self.maude.operators(
            [f"[{left_side}] <{parameter_term}>"], term_domain, "DState", []
        )

        preamble = f"""
            {variables_str}

            {locations_str}

            {source_term}
            {target_term}"""

        return preamble

    def _get_equations(self, clocks: list[str], parameters: list[str]) -> str:
        """
        Get the equations for interpreting states

        Parameters
        ----------
        clocks : list[str]
            clocks of the model
        parameters : list[str]
            parameters of the model

        Returns
        -------
        str
        """
        variable = self.maude.variables(["l"], "Location")

        left_term = " ; ".join(["l"] + clocks)

        right_term = " ; ".join(parameters)
        right_term = f"< {right_term} >" if len(parameters) else "<>"

        first_equation = self.maude.equation(
            f"get-location( < {left_term} > {right_term} )", "l", [], []
        )

        second_equation = self.maude.equation(
            f"get-location( [ {left_term} ] {right_term} )", "l", [], []
        )

        equations = f"""
            {variable}
            {first_equation}
            {second_equation}
        """

        return equations

    def _get_reachability(
        self,
        initial_location: str,
        clocks: list[str],
        parameters: list[str],
    ) -> str:
        """
        Get reachability command for verification

        Parameters
        ----------
        initial_location : str
            identifier of the initial location of the automaton
        clocks : list[str]
            clocks of the model
        parameters : list[str]
            parameters of the model

        Returns
        -------
        str
        """
        fresh_clocks = [f"{c}:{self.var_type}" for c in clocks]
        fresh_params = [f"{p}:{self.var_type}" for p in parameters]

        # initial state
        initial_state = self._get_state(
            initial_location, fresh_clocks, fresh_params, True
        )

        # initial conditions
        condition_1 = []
        if len(fresh_clocks) > 1:
            condition_1 = [f"{fresh_clocks[0]} === {c}" for c in fresh_clocks[1:]]

        condition_2 = [f"{v} >= 0/1" for v in fresh_clocks + fresh_params]
        condition = " and ".join(condition_1 + condition_2)

        cmd = f"red reachability (<replace>, ( {initial_state} | ( {condition} ) )) ."
        return cmd

    def to_maude(self, model: Model) -> str:
        """
        Parse an Imitator model into Maude

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        automaton = model.automata[0]
        clocks = model.clocks
        parameters = model.parameters

        rules = []
        for source in automaton.locations:
            for t in automaton.transitions_from(source.name):
                target = automaton.get_location(t.target)

                # update update variables
                updates = self._get_updated_variables(clocks, t.update)

                # add transition rule
                rules.append(
                    self._get_rule(source, target, t.guard, clocks, parameters, updates)
                )

            # add tick rule
            rules.append(self._get_tick_rule(source, clocks, parameters))
            rules.append("")

        rules = "\n            ".join(rules)

        load_pta = self.maude.loadFile("pta-base")
        load_meta = self.maude.loadFile("meta-pta")
        import_base = self.maude.importModule("MODEL-BASE", ImportType.INCLUDING)
        import_real = self.maude.importModule("REAL", ImportType.PROTECTING)
        preamble = self._get_preamble(automaton, clocks, parameters)
        equations = self._get_equations(clocks, parameters)
        reachability = self._get_reachability(
            automaton.initial_location.name, clocks, parameters
        )

        model_str = f"""\
        {load_pta}

        mod MODEL is
            {import_base}
            {import_real}
            {preamble}

            {rules}
            {equations}
        endm

        {load_meta}

        {reachability}

        quit .

        eof
        """

        return textwrap.dedent(model_str).strip()


class InterpreterParser(Parser):
    """A class to represent the parser of Imitator to Maude using the interpreter version"""

    def __init__(self) -> None:
        """Constructor of the class"""
        super().__init__()

    def _get_parameter_constraints(self, model) -> list[str]:
        """Get the initial constraints of parameters

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        list[str]
        """
        params = model.parameters
        params_constraints = []
        for c in model.initial_constraints:
            v1, op, v2 = self._normalize_constraints(c, False, True).split(" ")
            new_v1 = f"rr(var({v1}))" if v1 in params else v1
            new_v2 = f"rr(var({v2}))" if v2 in params else v2

            if new_v1.startswith("rr(") or new_v2.startswith("rr)"):
                params_constraints.append(f"{new_v1} {op} {new_v2}")

        return params_constraints

    def _get_discrete_constraints(self, model) -> list[str]:
        """Get the initial constraints of discrete variables

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        list[str]
        """
        vars = model.discrete_vars
        var_constraints = []
        for c in model.initial_constraints:
            v1, _, v2 = self._normalize_constraints(c, False).split(" ")
            if v1 in vars:
                var_constraints.append(f"{v1} : {v2}")

        return var_constraints

    def _normalize_constraints(
        self, constraint: str, parens: bool = True, expand_equal: bool = False
    ) -> str:
        """
        Normalize the string represention of a constraint. For instance, adding
        spaces around binary operators.

        Parameters
        ----------
        constraint : str
            string representation of the constraint
        parens : bool
            Flag to surround constraint with parenthesis
        expand_equal : bool
            Flag to use the maude symbol for equal

        Returns
        -------
        str
        """
        # replace True by true
        new_constr = "true" if constraint == "True" else constraint

        # add space around binary operators
        c_with_spaces = re.sub(r"((?!\.|\/)[^\w-]+)", r" \1 ", new_constr)

        # remove more than one space
        no_spaces = re.sub(" +", " ", c_with_spaces)

        # replace binary & by and
        result = no_spaces.replace(" | ", " or ").replace(" & ", " and ")

        if expand_equal:
            result = result.replace(" = ", " === ")

        if parens and result != "true":
            c_list = [f"({g})" for g in result.split(" and ")]
            result_str = " and ".join(c_list)
            result = f"({result_str})" if len(c_list) > 1 else result_str

        return result

    def _get_preamble(self, model: Model) -> str:
        """
        Get the preamble of the Maude file

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        # locations
        locations = sorted(
            [loc.name for automaton in model.automata for loc in automaton.locations]
        )
        locations_str = self.maude.operators(locations, [], "Location", ["ctor"])

        # actions
        actions = sorted(model.actions)
        actions_str = self.maude.operators(actions, [], "Action", ["ctor"])

        # automata label
        names = sorted([automaton.name for automaton in model.automata])
        names_str = self.maude.operators(names, [], "Agent", ["ctor"])

        # clocks
        clocks = sorted(model.clocks)
        clocks_str = self.maude.operators(clocks, [], "Clock", ["ctor"])

        # parameters
        parameters = sorted(model.parameters)
        parameters_str = self.maude.operators(parameters, [], "Parameter", ["ctor"])

        # discrete variables
        discrete_vars = sorted(model.discrete_vars)
        discrete_str = self.maude.operators(discrete_vars, [], "DVariable", ["ctor"])

        preamble = f"""
            {locations_str}
            {actions_str}
            {names_str}
            {clocks_str}
            {discrete_str}
            {parameters_str}"""

        # remove empty lines
        preamble = "\n".join([s for s in preamble.split("\n") if s.strip()])
        return preamble.strip()

    def _get_init_eq(self, model: Model) -> str:
        """Get the initial state of the Maude interpreter

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        label = "init"
        init_op = self.maude.operators([label], [], "State", [])

        # locs
        locs = [f"{a.name} @ {a.initial_location.name}" for a in model.automata]
        locs_str = f"({', '.join(locs)})"

        # clocks
        # TODO: check this with Carlos
        clocks = [f"{clock} : 0" for clock in model.clocks]
        clocks_str = f"({', '.join(clocks)})" if len(clocks) else "empty"

        # params
        # TODO: check this with Carlos
        params = [f"{param} : rr(var({param}))" for param in model.parameters]
        params_str = f"({', '.join(params)})" if len(params) else "empty"

        # variables
        variables = self._get_discrete_constraints(model)
        vars_str = f"({', '.join(variables)})" if len(variables) else "empty"

        # constraints
        constraints = self._get_parameter_constraints(model)
        constraints_str = " and ".join(constraints)

        tab = " " * 22
        config = f"\n{tab}".join(
            [
                "tick: tickOk",
                f"  locs: {locs_str}",
                f"  clocks: {clocks_str}",
                f"  parameters: {params_str}",
                f"  dvariables: {vars_str}",
                f"  constraint: {constraints_str}",
                "  n: 1",
            ]
        )

        agents = sorted([automaton.name.title() for automaton in model.automata])
        agents_str = ", ".join(agents)
        init_eq_str = f"eq {label} = {{ {agents_str} }}\n{tab}< {config} >\n{tab}."

        eq = f"""
            {init_op}
            {init_eq_str}"""

        return eq

    def _get_transition(self, t: Transition, sync_actions: list[str]) -> str:
        """Get the enconding of the transition of an automaton

        Parameters
        ----------
        t : Transition
            automaton transition
        sync_actions : list[str]
            list of actions that synchronise in the automata

        Returns
        -------
        str
        """
        # updates
        updates = " ; ".join(
            [re.sub(r"(\S+)\s*:=\s*(\S+)", r"\1 := \2", u) for u in t.update]
        )
        updates_str = "nothing" if updates == "" else updates

        # sync
        actions = t.sync
        sync_str = f"sync {actions}" if actions in sync_actions else f"local {actions}"

        # guard
        guard = self._normalize_constraints(t.guard)

        return f"when {guard} {sync_str} do {{ {updates_str} }} goto {t.target}"

    def _get_automaton(self, automaton: Automaton, sync_actions: list[str]) -> str:
        """Get the encoding of an automaton in the Maude interpreter

        Parameters
        ----------
        automaton : Automaton
            Imitator model
        sync_actions : list[str]
            list of actions that synchronise in the automata

        Returns
        -------
        str
        """
        name = automaton.name
        tab = " " * 26

        op = self.maude.operators([name.title()], [], "Automaton", [])

        # add states with their transitions
        locations = []
        for loc in automaton.locations:
            trans = [
                self._get_transition(t, sync_actions)
                for t in automaton.transitions_from(loc.name)
            ]

            trans_str = f" ,\n{tab}     ".join(trans)
            trans_str = f"\n{tab}    ({trans_str})" if len(trans) else "empty"

            inv = self._normalize_constraints(loc.invariant)
            loc = f"(@ {loc.name} inv {inv} : {trans_str})"
            locations.append(loc)
        locations_str = f",\n{tab} ".join(locations)

        eq_str = f"eq {name.title()} = < {name} |\n{tab} {locations_str} >\n{tab}."

        result = f"""
            {op}
            {eq_str}"""

        return result

    def _get_synthesis_cmd(self, nb_traces: int = None, nb_steps: int = None) -> str:
        """Return the command for parameter synthesis

        Parameters
        ----------
        nb_traces : int
            number of traces to be found in the synthesis analysis
        nb_steps : int
            number of steps of the synthesis analysis
        """
        params = []
        if nb_traces is not None:
            params.append(str(nb_traces))
        if nb_steps is not None:
            params.append(str(nb_steps))

        params = f"[ {' ; '.join(params)} ]"

        cmd = f"red synthesis {params} in 'MODEL : init => (<replace>) ."
        return cmd

    def _get_search_cmd(self, nb_traces: int = None, nb_steps: int = None) -> str:
        """Return the command for reachability analysis

        Parameters
        ----------
        nb_traces : int
            number of traces to be found in the synthesis analysis
        nb_steps : int
            number of steps of the synthesis analysis
        """
        params = []
        if nb_traces is not None:
            params.append(str(nb_traces))
        if nb_steps is not None:
            params.append(str(nb_steps))

        params = f"[ {' ; '.join(params)} ]"

        cmd = f"red search-folding {params} in 'MODEL : init => (<replace>) ."
        return cmd

    def to_maude(self, model: Model) -> str:
        """Parse an Imitator model into Maude

        Parameters
        ----------
        model : Model
            Imitator model

        Returns
        -------
        str
        """
        initial_constraints = model.initial_constraints
        if len(initial_constraints):
            print("Please check the initial constraints in the search command:")
            print(f"    {initial_constraints}")

        load = self.maude.loadFile("analysis")
        import_dynamics = self.maude.importModule("ANALYSIS", ImportType.EXTENDING)
        preamble = self._get_preamble(model)

        init = self._get_init_eq(model)

        actions = [
            action for a in model.automata for action in {t.sync for t in a.transitions}
        ]
        sync_actions = list(set([a for a in actions if actions.count(a) > 1]))

        equations = "\n".join(
            [self._get_automaton(a, sync_actions) for a in model.automata]
        )

        synthesis_cmd = self._get_synthesis_cmd(1)
        search_cmd = self._get_search_cmd(1)

        model_str = f"""\
        {load}

        mod MODEL is
            {import_dynamics}

            {preamble}
            {equations}
            {init}
        endm

        {synthesis_cmd}
        {search_cmd}

        quit .

        eof
        """

        return textwrap.dedent(model_str).strip()
