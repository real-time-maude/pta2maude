from collections import defaultdict
from itertools import product
from typing import Union


class Location:
    """
    A class to represent a location of an automaton

    Attributes
    ----------
    name : str
        location's name
    invariant : str
        locations's invariant
    initial : bool
        flag to mark a location as an initial location
    accepting : bool
        flag to mark a location as an accepting location
    urgent : bool
        flag to mark a location as an urgent location
    """

    def __init__(
        self, name: str, invariant: str, initial: bool, accepting: bool, urgent: bool
    ):
        """
        Constructor of the class

        Parameters
        ----------
        name : str
            location's name
        invariant : str
            locations's invariant
        initial : bool
            flag to mark a location as an initial location
        accepting : bool
            flag to mark a location as an accepting location
        urgent : bool
            flag to mark a location as an urgent location
        """
        self.name = str(name)
        self.invariant = str(invariant)
        self.initial = bool(initial)
        self.accepting = bool(accepting)
        self.urgent = bool(urgent)

    def __str__(self):
        marks = []
        if self.initial:
            marks.append("->")
        if self.accepting:
            marks.append("*")
        if self.urgent:
            marks.append("!")

        marks_str = "[" + ",".join(marks) + "]" if len(marks) else ""
        return f"Location{marks_str} {self.name} ({self.invariant})"

    def __repr__(self):
        return str(self)


class Transition:
    """
    A class to represent a transition of an automaton

    Attributes
    ----------
    source : str
        Identifier of the source state
    guard : str
        The guard of the transition
    update : list[str]
        The updates of the transition
    sync : str
        The synchronization label of the transition
    target : str
        Identifier of the target state
    """

    def __init__(
        self, source: str, guard: str, update: list[str], sync: str, target: str
    ):
        """
        Constructor of the class

        Parameters
        ----------
        source : str
            Identifier of the source state
        guard : str
            The guard of the transition
        update : list[str]
            The updates of the transition
        sync : str
            The synchronization label of the transition
        target : str
            Identifier of the target state
        """
        self.source = str(source) if source is not None else None
        self.guard = str(guard) if guard is not None else None
        self.update = list(update) if update is not None else []
        self.sync = str(sync) if sync is not None else None
        self.target = str(target) if target is not None else None

    def __str__(self):
        return f"Transition({self.source} |-[{self.guard}, {self.update}, {self.sync})]-> {self.target}"

    def __repr__(self):
        return str(self)


class Automaton:
    """
    A class to represent an automaton

    Attributes
    ----------
    name : str
        name of the automaton
    actions : list[str]
        actions of the automaton
    locations :  list[Location]
        locations of the automaton
    transitions : list[Transition]
        transitions of the automaton
    initial_location : Location
        initial location of the automaton
    accepting_locations : list[Location]
        accepting locations of the automaton
    urgent_locations : list[Location]
        urgent locations of the automaton
    """

    def __init__(self, name: str, actions: list[str]):
        """
        Constructor of the class

        Parameters
        ----------
        name : str
            name of the automaton
        actions : list[str]
            synchronisation labels
        """
        self.name = str(name)
        self.actions = list(actions)
        self._locations = defaultdict()
        self._transitions = defaultdict(list)

    def add_location(self, location: Location):
        """
        Add a new location to the automaton.

        Parameters
        ----------
        location : Location
            the new location
        """
        self._locations[location.name] = location

    def add_transition(self, transition: Transition):
        """
        Add a new transition to the automaton.

        Parameters
        ----------
        transition : Transition
            the new transition
        """
        self._transitions[transition.source].append(transition)

    @property
    def locations(self):
        return list(self._locations.values())

    @property
    def transitions(self):
        return [t for t_list in self._transitions.values() for t in t_list]

    @property
    def initial_location(self):
        return next((loc for loc in self.locations if loc.initial), None)

    @property
    def accepting_locations(self):
        return [loc for loc in self.locations if loc.accepting]

    @property
    def urgent_locations(self):
        return [loc for loc in self.locations if loc.urgent]

    def get_location(self, name: str) -> Location:
        """
        Get a location by its name.

        Parameters
        ----------
        name : str
            locations's name

        Returns
        -------
        Location
        """
        return self._locations[name]

    def transitions_from(self, source: str) -> list[Transition]:
        """Get transitions from a given location.

        Parameters
        ----------
        source : str
            Location'name

        Returns
        -------
        list[Transition]
            list of transitions
        """
        return list(self._transitions[source])

    def sync_transitions(self, action: str) -> list[Transition]:
        """Get transitions synchronized by an action"""
        return [t for t in self.transitions if t.sync == action]

    def __str__(self):
        return f"Automaton({self.name}, {self.actions}, {self.locations})"

    def __repr__(self):
        return str(self)


class Model:
    """
    Class representing an Imitator model

    Attributes
    ----------
    automata: list[Automaton]
        List of automata composing the model
    parameters: list[str]
        List of parameters of the model
    clocks: list[str]
        List of clocks of the model
    actions: list[str]
        List of actions of the model
    initial_constraints : list[str]
        List of initial parameter constraints
    """

    def __init__(self):
        """Constructor of the class"""
        self.automata = list()
        self.parameters = list()
        self.clocks = list()
        self.discrete_vars = list()
        self.actions = list()
        self.initial_constraints = list()

    def add_parameter(self, param: Union[str, list[str]]):
        """
        Add a parameter or a list of parameters to the model

        Parameters
        ----------
        param : list[str] or str
            a parameter or a list of parameters to be added
        """
        if isinstance(param, list):
            self.parameters.extend(param)
        else:
            self.parameters.append(param)

    def add_clock(self, clock: Union[str, list[str]]):
        """
        Add a clock or a list of clocks to the model

        Parameters
        ----------
        clock : list[str] or str
            a clock or a list of clocks to be added
        """
        if isinstance(clock, list):
            self.clocks.extend(clock)
        else:
            self.clocks.append(clock)

    def add_discrete_var(self, var: Union[str, list[str]]):
        """
        Add a discrete variable or a list of discrete variable to the model

        Parameters
        ----------
        var : list[str] or str
            a discrete variable or a list of discrete variables to be added
        """
        if isinstance(var, list):
            self.discrete_vars.extend(var)
        else:
            self.discrete_vars.append(var)

    def add_automaton(self, automaton: Union[Automaton, list[Automaton]]):
        """
        Add an automaton or a list of automata to the model

        Parameters
        ----------
        automaton : list[Automaton] or Automaton
            an automaton or a list of automata to be added
        """
        if isinstance(automaton, list):
            self.automata.extend(automaton)
            self.actions.extend([actions for a in automaton for actions in a.actions])
        else:
            self.automata.append(automaton)
            self.actions.extend(automaton.actions)
        self.actions = list(set(self.actions))

    def add_initial_constraint(self, constraint: Union[str, list[str]]):
        """
        Add an initial constraint or a list of initial constraints to the model

        Parameters
        ----------
        constraint : list[str] or str
            a constraint or a list of constraints to be added
        """
        if isinstance(constraint, list):
            self.initial_constraints.extend(constraint)
        else:
            self.initial_constraints.append(constraint)

    def get_automaton(self, name: str) -> Automaton:
        """
        Get an automaton by its name

        Parameters
        ----------
        name : str
            automaton's name

        Returns
        -------
        Automaton
        """
        return next((a for a in self.automata if a.name == name), None)

    def remove_unreachable_states(self, automaton: Automaton) -> Automaton:
        """
        Returns the automaton without the unreachable locations

        Parameters
        ----------
        automaton : Automaton
            automaton to be simplified

        Returns
        Automaton
        """
        final_automaton = Automaton(automaton.name, automaton.actions)

        stack = [automaton.initial_location.name]
        visited = set()
        while stack:
            location = stack.pop()
            if location in visited:
                continue

            final_automaton.add_location(automaton.get_location(location))
            visited.add(location)
            for t in automaton.transitions_from(location):
                stack.append(t.target)
                final_automaton.add_transition(t)

        return final_automaton

    def _merge_constraints(self, constraint: list[str]) -> str:
        """Merge a list of constraint"""
        new_constraint = [c for c in constraint if c != "True"]
        new_constraint = new_constraint if len(new_constraint) else ["True"]

        return " & ".join(new_constraint)

    def _get_composed_location_name(self, locations: list[str]) -> str:
        """Returns the new name of a composed location"""
        return "".join([loc.capitalize() for loc in locations])

    def get_synchronized_product(self, name: str = "sync_prod") -> Automaton:
        """
        Computes the synchronized product of the automata of the model.

        Parameters
        ----------
        name : str
            Name of the generated automaton

        Returns
        -------
        Automaton
        """
        model = Model()

        # union of clocks, parameters and initial constraints
        model.clocks = self.clocks
        model.parameters = self.parameters
        model.initial_constraints = self.initial_constraints

        # union of actions
        automaton = Automaton(name, self.actions)

        # product of locations
        new_locations = product(*[a.locations for a in self.automata])
        for locs_tuple in new_locations:
            # check if the location is an initial location
            initial = all([loc.initial for loc in locs_tuple])

            # check if the location is an accepting location
            accepting = any([loc.accepting for loc in locs_tuple])

            # check if the location is an urgent location
            urgent = any([loc.urgent for loc in locs_tuple])

            # compute the new invariant of the location
            invariant = self._merge_constraints([loc.invariant for loc in locs_tuple])

            # define a new name for the location
            name = self._get_composed_location_name([loc.name for loc in locs_tuple])

            # add location to the automaton
            automaton.add_location(
                Location(name, invariant, initial, accepting, urgent)
            )

        # add transitions
        for act in self.actions:
            sync_transitions = [a.sync_transitions(act) for a in self.automata]
            sync_transitions = [
                [None] if not len(t_list) else t_list for t_list in sync_transitions
            ]

            # generate all the posible transitions
            sync_transitions = [
                list(
                    zip(
                        *[
                            [None, None, None, []]
                            if t is None
                            else [t.source, t.target, t.guard, t.update]
                            for t in sync
                        ]
                    )
                )
                for sync in product(*sync_transitions)
            ]

            for t in sync_transitions:
                source_t, target_t, guard_t, update_t = t
                guard = self._merge_constraints([g for g in guard_t if g is not None])
                update = [u for u_list in update_t for u in u_list]

                # add transition directly if all locations synchronize
                if not (None in source_t):
                    automaton.add_transition(
                        Transition(
                            self._get_composed_location_name(source_t),
                            guard,
                            update,
                            act,
                            self._get_composed_location_name(target_t),
                        )
                    )
                else:
                    # add all the transitions that do not synchronize
                    indices = [i for i, x in enumerate(source_t) if x is None]

                    source = list(source_t)
                    target = list(target_t)
                    for i in range(len(self.automata)):
                        if i in indices:
                            e = [loc.name for loc in self.automata[i].locations]
                            source[i] = e
                            target[i] = e
                        else:
                            source[i] = [source[i]]
                            target[i] = [target[i]]

                    source = [
                        self._get_composed_location_name(p) for p in product(*source)
                    ]
                    target = [
                        self._get_composed_location_name(p) for p in product(*target)
                    ]
                    new_transitions = list(zip(*[source, target]))

                    # add the rest of transitions
                    for new_t in new_transitions:
                        automaton.add_transition(
                            Transition(new_t[0], guard, update, act, new_t[1])
                        )

        # remove unreachable states
        final_automaton = self.remove_unreachable_states(automaton)

        # the global model has only one automaton
        model.add_automaton(final_automaton)
        return model

    def __str__(self):
        return f"clocks: {self.clocks}\nparameters: {self.parameters}\ndiscrete: {self.discrete_vars}\nactions: {self.actions}\nautomata: {self.automata}"

    def __repr__(self):
        return str(self)
