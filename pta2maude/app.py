import argparse

from src.Parser import CollapsingParser, InterpreterParser, NoCollapsingParser


def main(input_filename, output_filename, version):
    """
    Parse an Imitator file

    Parameters
    ----------
    input_filename : str
        Imitator filename
    output_filename : str
        Path of the output file
    version :str
        Version of the maude encoding
    """
    try:
        # parse Imitator file
        if version == "collapsing":
            parser = CollapsingParser()
            sync_product = True
        elif version == "no-collapsing":
            parser = NoCollapsingParser()
            sync_product = True
        elif version == "interpreter":
            parser = InterpreterParser()
            sync_product = False
        else:
            raise Exception(f"the version '{version}' is not supported")

        output = parser.parse_file(input_filename, sync_product)

        # write to file
        with open(output_filename, "w") as f:
            f.write(output)
            print(f"Output file: {output_filename}")
    except Exception as E:
        print(f"Error processing {input_filename}.\n {E}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""From Imitator to Maude specifications""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--output", type=str, default="./model.maude", help="Maude output file"
    )

    parser.add_argument(
        "--version",
        type=str,
        choices=["collapsing", "no-collapsing", "interpreter"],
        required=True,
        help="Version of the Maude encoding",
    )

    parser.add_argument("--input", type=str, required=True, help="Imitator file")

    args = parser.parse_args()
    main(args.input, args.output, args.version)
